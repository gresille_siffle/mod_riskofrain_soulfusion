-- if an item is picked by a player
-- all the others get the item too
registercallback("onItemPickup", function(item_inst, player)
	local item = item_inst:getItem()

	for index, a_player in ipairs(misc.players) do

		if a_player ~= nil and a_player:isValid() then

			if player ~= a_player and item.isUseItem == false then
				a_player:giveItem(item, 1)
			end
		end
	end
end)

-- if a player dies
-- all the others die too
registercallback("onPlayerDeath", function(dead_player)
	for index, a_player in ipairs(misc.players) do

	    if a_player ~= nil and a_player:isValid() then
		
			-- I should check if a_player is already dead
		    if a_player ~= dead_player == true then
				a_player:kill()
			end
		end
	end
end)
